# hackernews

Este proyecto es la implementacion del tutorial propuesto en how to graphQL, Hackernews usando Django y Graphene.
La plataforma tendrá las siguientes características:

*  Lista de Usuarios y Enlaces

*  Creación y autenticación de usuarios.

*  Los usuarios pueden crear enlaces y votar sobre ellos.
